/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class DivideMultiplyTest {
    
    public DivideMultiplyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of multiply method, of class DivideMultiply.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        double a = 6.9;
        DivideMultiply instance = new DivideMultiply(a);
        instance.multiply(a);
    }

    /**
     * Test of divide method, of class DivideMultiply.
     */
    @Test
    public void testDivide() {
        System.out.println("divide");
        double a = 7.8;
        DivideMultiply instance = new DivideMultiply(a);
        instance.divide(a);
    }

    /**
     * Test of getCurrentVal method, of class DivideMultiply.
     */
    @Test
    public void testGetCurrentVal() {
        System.out.println("getCurrentVal");
        double a = 4.6;
        DivideMultiply instance = new DivideMultiply(a);
        double expResult = 4.6;
        double result = instance.getCurrentVal();
        assertEquals(expResult, result, 0.0);
    }
}