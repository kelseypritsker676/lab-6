/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class AlarmTest {
    
    public AlarmTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getHour method, of class Alarm.
     */
    @Test
    public void testGetHour() {
        System.out.println("getHour");
        Alarm instance = new Alarm();
        int expResult = 0;
        int result = instance.getHour();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMinute method, of class Alarm.
     */
    @Test
    public void testGetMinute() {
        System.out.println("getMinute");
        Alarm instance = new Alarm();
        int expResult = 0;
        int result = instance.getMinute();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTime method, of class Alarm.
     */
    @Test
    public void testSetTime() {
        System.out.println("setTime");
        int hr = 3;
        int min = 30;
        Alarm instance = new Alarm();
        boolean expResult = true;
        boolean result = instance.setTime(hr, min);
        assertEquals(expResult, result);
        assertTrue(instance.setTime(7,30));
        assertEquals(7,instance.getHour());
        assertEquals(30,instance.getMinute());
    }

    /**
     * Test of toString method, of class Alarm.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Alarm instance = new Alarm();
        String expResult = "12:00AM";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}