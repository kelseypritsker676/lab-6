/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class AddSubtractTest {
    
    public AddSubtractTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of add method, of class AddSubtract.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        int a = 5;
        AddSubtract instance = new AddSubtract(a);
        instance.add(a);
    }

    /**
     * Test of subtract method, of class AddSubtract.
     */
    @Test
    public void testSubtract() {
        System.out.println("subtract");
        int a = 6;
        AddSubtract instance = new AddSubtract(a);
        instance.subtract(a);
    }

    /**
     * Test of getCurrentVal method, of class AddSubtract.
     */
    @Test
    public void testGetCurrentVal() {
        System.out.println("getCurrentVal");
        int a=7;
        AddSubtract instance = new AddSubtract(a);
        int expResult = 7;
        int result = instance.getCurrentVal();
        assertEquals(expResult, result);
    }
}