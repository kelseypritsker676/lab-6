/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class AcuteTriangleTest {
    
    public AcuteTriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isValid method, of class AcuteTriangle.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        AcuteTriangle instance = new AcuteTriangle(9,9,9);
        boolean expResult = true;
        boolean result = instance.isValid();
        assertEquals(expResult, result);
    }
}