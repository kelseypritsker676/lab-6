/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class EquilateralTriangleTest {
    
    public EquilateralTriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isValid method, of class EquilateralTriangle.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        EquilateralTriangle instance = new EquilateralTriangle(4,4,4);
        boolean expResult = true;
        boolean result = instance.isValid();
        assertEquals(expResult, result);
    }
}