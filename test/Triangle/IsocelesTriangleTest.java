/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class IsocelesTriangleTest {
    
    public IsocelesTriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isValid method, of class IsocelesTriangle.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        IsocelesTriangle instance = new IsocelesTriangle(4,4,3);
        boolean expResult = true;
        boolean result = instance.isValid();
        assertEquals(expResult, result);
    }
}