/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;

import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class TriangleTest {
    
    public TriangleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of perimeter method, of class Triangle.
     */
    @Test
    public void testPerimeter() {
        System.out.println("perimeter");
        Triangle instance = new Triangle(3,3,3);
        double expResult = 9.0;
        double result = instance.perimeter();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getLengths method, of class Triangle.
     */
    @Test
    public void testGetLengths() {
        System.out.println("getLengths");
        Triangle instance = new Triangle(3,3,3);
        ArrayList expResult = new ArrayList();
        expResult.add(3.0);
        expResult.add(3.0);
        expResult.add(3.0);
        ArrayList result = instance.getLengths();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAngles method, of class Triangle.
     */
    @Test
    public void testGetAngles() {
        System.out.println("getAngles");
        Triangle instance = new Triangle(3,3,3);
        ArrayList expResult = new ArrayList();
        expResult.add(60.0);
        expResult.add(60.0);
        expResult.add(60.0);
        ArrayList result = instance.getAngles();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAnglesDegrees method, of class Triangle.
     */
    @Test
    public void testGetAnglesDegrees() {
        System.out.println("getAnglesDegrees");
        Triangle instance = new Triangle(3,3,3);
        ArrayList expResult = new ArrayList();
        expResult.add(60.0);
        expResult.add(60.0);
        expResult.add(60.0);
        ArrayList result = instance.getAngles();
        assertEquals(expResult, result);
    }

    /**
     * Test of isValid method, of class Triangle.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        Triangle instance = new Triangle(3,3,3);
        boolean expResult = true;
        boolean result = instance.isValid();
        assertEquals(expResult, result);
    }

    /**
     * Test of area method, of class Triangle.
     */
    @Test
    public void testArea() {
        System.out.println("area");
        Triangle instance = new Triangle(2,2,2);
        double expResult = Math.sqrt(3.0);
        double result = instance.area();
        assertEquals(expResult, result, 0.0);
    }
}