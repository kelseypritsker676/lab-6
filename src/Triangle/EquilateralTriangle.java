/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;

/**
 *
 * @author kelsey.pritsker676
 */
public class EquilateralTriangle extends AcuteTriangle{
    public EquilateralTriangle(double sa, double sb, double sc){
        super(sa,sb,sc);
    }
    
    public boolean isValid(){
        boolean valid = false;
        if(super.isValid() && sa==sb && sb==sc && sc==sa){
            valid = true;
        }
        return valid;
    }
}
