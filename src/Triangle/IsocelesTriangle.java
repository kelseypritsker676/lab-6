/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;

/**
 *
 * @author kelsey.pritsker676
 */
public class IsocelesTriangle extends AcuteTriangle{
    public IsocelesTriangle(double sa, double sb, double sc){
        super(sa,sb,sc);
    }
    
    public boolean isValid(){
        boolean valid = false;
        if(super.isValid()){
            if(sa==sb || sb==sc || sc==sa){
                valid = true;
            }
        }
        return valid;
    }
}
