/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;

/**
 *
 * @author kelsey.pritsker676
 */
public class PythagoreanTriangle extends RightAngle{
    public PythagoreanTriangle(double sa, double sb, double sc){
        super(sa,sb,sc);
    }
    
    public boolean isValid(){
        boolean valid = false;
        if(super.isValid()){
            if(Math.pow((int)sa, 2)+Math.pow((int)sb,2)==Math.pow((int)sc, 2) || 
                    Math.pow((int)sc, 2)+Math.pow((int)sb,2)==Math.pow((int)sa, 2) ||
                    Math.pow((int)sa, 2)+Math.pow((int)sc,2)==Math.pow((int)sb, 2)){
                valid = true;
            }
        }
        return valid;
    }
}
