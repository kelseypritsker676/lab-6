/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;
import java.util.*;

/**
 *
 * @author kelsey.pritsker676
 */
public class Triangle {
    protected double e,sa,sb,sc,aa,ab,ac;
    //e=error, sa=side a, sb=side b, sc=side c, aa=angle a, ab=angle b, ac=angle c
    
    private boolean eq(double a, double b){
        if(a==b)
            return true;
        else
            return false;
    }
    
    private boolean lt(double a, double b){
        if(a<b)
            return true;
        else
            return false;
    }
    
    public Triangle(double sa, double sb, double sc){
        this.sa = sa;
        this.sb = sb;
        this.sc = sc;
        //use Law of Cosines to find angles
        aa=Math.rint(Math.toDegrees(Math.acos((Math.pow(this.sb,2)+Math.pow(this.sc,2)-Math.pow(this.sa,2))/(2*this.sb*this.sc))));
        ab=Math.rint(Math.toDegrees(Math.acos((Math.pow(this.sa,2)+Math.pow(this.sc,2)-Math.pow(this.sa,2))/(2*this.sa*this.sc))));
        ac=Math.rint(Math.toDegrees(Math.acos((Math.pow(this.sb,2)+Math.pow(this.sa,2)-Math.pow(this.sc,2))/(2*this.sb*this.sa))));
    }
    
    public double perimeter(){
        double per = sa+sb+sc;
        return per;
    }
    
    public ArrayList getLengths(){
        ArrayList len = new ArrayList();
        len.add(this.sa);
        len.add(this.sb);
        len.add(this.sc);
        return len;
    }
    
    public ArrayList getAngles(){
        ArrayList len = new ArrayList();
        len.add(this.aa);
        len.add(this.ab);
        len.add(this.ac);
        return len;
    }
    
    public ArrayList getAnglesDegrees(){
        ArrayList len = new ArrayList();
        len.add(this.aa);
        len.add(this.ab);
        len.add(this.ac);
        return len;
    }
    
    public boolean isValid(){
        boolean valid = true;
        double totalDegrees = aa+ab+ac;
        e=180;
        if(totalDegrees<0 || totalDegrees>e || (sa+sb)<sc || (sb+sc)<sa || (sa+sc)<sb)
            valid=false;
        return valid;
    }
    
    public double area(){
        double s= (sa+sb+sc)/2;
        double area = Math.sqrt(s*(s-sa)*(s-sb)*(s-sc));
        return area;
    }
}
