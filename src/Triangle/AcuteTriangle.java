/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle;


/**
 *
 * @author kelsey.pritsker676
 */
public class AcuteTriangle extends Triangle{
    public AcuteTriangle(double sa, double sb, double sc){
        super(sa,sb,sc);
    }
    
    public boolean isValid(){
        boolean valid = false;
        if(super.isValid() && aa<90 && ab<90 && ac<90){
            valid = true;
        }
        return valid;
    }
}
