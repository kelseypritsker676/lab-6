/**
 *
 * @author 
 */
public class AddSubtract {
    

     private int currentVal;
  
     /**
     * Constructor to initialize our member variable.
      */
    public AddSubtract(int a) {
       currentVal = a;
     }

    /**
     * Add the value of 'a' to our current value.
    */
   public void add(int a) {
      currentVal += a;
   }

    /**
    * Subtract the value of 'a' from our current value.
     */
    public void subtract(int a) {
       currentVal -= a;
   }

    /**
     * Get the current value.
     */
    public int getCurrentVal() {
      return currentVal;
    }
 }