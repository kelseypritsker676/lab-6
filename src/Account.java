
/**
 * The account class acts like a banking application.

 */
public class Account {

    /**
     * Holds the current balance.
     */
    public double balance;

    /**
     * Constructor for empty balance.
     */
   public Account() {
        balance = 0.0;
   }

    /**
     * Deposits money in the bank.
     */
    public void deposit( double amount ) 
    {
    	balance += amount;
    }

    /**
     * Withdrawls money from the bank.
     * @param amount the amount to withdrawl
     * @returns 0 if it can't be done, otherwise the amount specified
     */
    public double withdraw( double amount )
    {
        // See if amount can be withdrawn
    	if (balance >= amount) 
        {
    		balance -= amount;
               return amount;
    	}
    	else {
            // Withdrawal not allowed
            return 0.0;
    	}
    }
 
    /**
     * Returns the balance in the bank.
     * @returns the current balance
     */
    public double getBalance() {
        return balance;
    }
}